# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'RoomType'
        db.create_table('lock_room_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('lock', ['RoomType'])

        # Adding model 'Room'
        db.create_table('lock_room', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.RoomType'])),
            ('department', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.Department'])),
        ))
        db.send_create_signal('lock', ['Room'])

        # Adding model 'ItemType'
        db.create_table('lock_item_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('lock', ['ItemType'])

        # Adding M2M table for field keys on 'ItemType'
        db.create_table('lock_item_type_keys', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('itemtype', models.ForeignKey(orm['lock.itemtype'], null=False)),
            ('key', models.ForeignKey(orm['lock.key'], null=False))
        ))
        db.create_unique('lock_item_type_keys', ['itemtype_id', 'key_id'])

        # Adding model 'Item'
        db.create_table('lock_item', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('serial_number', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('inventory_number', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('price', self.gf('django.db.models.fields.IntegerField')()),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.ItemType'])),
            ('room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.Room'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='related_items', null=True, to=orm['lock.Item'])),
        ))
        db.send_create_signal('lock', ['Item'])

        # Adding model 'ItemUserType'
        db.create_table('lock_item_user_type', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('lock', ['ItemUserType'])

        # Adding model 'ItemUser'
        db.create_table('lock_item__to__user', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.Item'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['trainman.UserProfile'])),
            ('type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.ItemUserType'])),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('lock', ['ItemUser'])

        # Adding model 'Key'
        db.create_table('lock_key', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal('lock', ['Key'])

        # Adding model 'Property'
        db.create_table('lock_property', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=1024)),
            ('key', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.Key'])),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['lock.Item'])),
        ))
        db.send_create_signal('lock', ['Property'])

        # Adding model 'LockAdmin'
        db.create_table('lock_admin', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user_profile', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['trainman.UserProfile'], unique=True)),
        ))
        db.send_create_signal('lock', ['LockAdmin'])

        # Adding M2M table for field departments on 'LockAdmin'
        db.create_table('lock_admin_departments', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('lockadmin', models.ForeignKey(orm['lock.lockadmin'], null=False)),
            ('department', models.ForeignKey(orm['trainman.department'], null=False))
        ))
        db.create_unique('lock_admin_departments', ['lockadmin_id', 'department_id'])


    def backwards(self, orm):
        # Deleting model 'RoomType'
        db.delete_table('lock_room_type')

        # Deleting model 'Room'
        db.delete_table('lock_room')

        # Deleting model 'ItemType'
        db.delete_table('lock_item_type')

        # Removing M2M table for field keys on 'ItemType'
        db.delete_table('lock_item_type_keys')

        # Deleting model 'Item'
        db.delete_table('lock_item')

        # Deleting model 'ItemUserType'
        db.delete_table('lock_item_user_type')

        # Deleting model 'ItemUser'
        db.delete_table('lock_item__to__user')

        # Deleting model 'Key'
        db.delete_table('lock_key')

        # Deleting model 'Property'
        db.delete_table('lock_property')

        # Deleting model 'LockAdmin'
        db.delete_table('lock_admin')

        # Removing M2M table for field departments on 'LockAdmin'
        db.delete_table('lock_admin_departments')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'lock.item': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Item'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'inventory_number': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'related_items'", 'null': 'True', 'to': "orm['lock.Item']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'price': ('django.db.models.fields.IntegerField', [], {}),
            'properties': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['lock.Key']", 'null': 'True', 'through': "orm['lock.Property']", 'blank': 'True'}),
            'room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.Room']"}),
            'serial_number': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.ItemType']"}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.UserProfile']", 'null': 'True', 'through': "orm['lock.ItemUser']", 'blank': 'True'})
        },
        'lock.itemtype': {
            'Meta': {'object_name': 'ItemType', 'db_table': "'lock_item_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'keys': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['lock.Key']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'lock.itemuser': {
            'Meta': {'object_name': 'ItemUser', 'db_table': "'lock_item__to__user'"},
            'date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.Item']"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.ItemUserType']"}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.UserProfile']"})
        },
        'lock.itemusertype': {
            'Meta': {'object_name': 'ItemUserType', 'db_table': "'lock_item_user_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'lock.key': {
            'Meta': {'object_name': 'Key'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'lock.lockadmin': {
            'Meta': {'object_name': 'LockAdmin', 'db_table': "'lock_admin'"},
            'departments': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['trainman.Department']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_profile': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['trainman.UserProfile']", 'unique': 'True'})
        },
        'lock.property': {
            'Meta': {'object_name': 'Property'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.Item']"}),
            'key': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.Key']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '1024'})
        },
        'lock.room': {
            'Meta': {'object_name': 'Room'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['lock.RoomType']"})
        },
        'lock.roomtype': {
            'Meta': {'object_name': 'RoomType', 'db_table': "'lock_room_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'})
        },
        'trainman.department': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Department'},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'db_column': "'id_department'", 'to': "orm['trainman.Department']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_column': "'name'"}),
            'type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.DepartmentType']", 'null': 'True', 'db_column': "'id_type'", 'blank': 'True'})
        },
        'trainman.departmenttype': {
            'Meta': {'object_name': 'DepartmentType', 'db_table': "'trainman_department_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256', 'db_column': "'name'"})
        },
        'trainman.userprofile': {
            'Meta': {'ordering': "('user__last_name',)", 'object_name': 'UserProfile', 'db_table': "'trainman_user_profile'"},
            'department': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['trainman.Department']", 'null': 'True', 'db_column': "'id_department'", 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pesel': ('django.db.models.fields.CharField', [], {'max_length': '11', 'null': 'True', 'db_column': "'pesel'", 'blank': 'True'}),
            'second_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'db_column': "'second_name'", 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'db_column': "'user'"})
        }
    }

    complete_apps = ['lock']