# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

urlpatterns = patterns('',
    (r'item/(?P<id_item>\d+)', 'apps.lock.views.show_item'),
    (r'print_receipt/(?P<id_item>\d+)', 'apps.lock.views.print_receipt'),    
    (r'', 'apps.lock.views.index')
)
